/***includes***/
#include<stdio.h>
#include<termios.h>
#include<unistd.h>
#include<stdlib.h>
#include<ctype.h>
#include<errno.h>
#include <sys/ioctl.h>
#include<string.h>
#include<sys/types.h>
#include"list.h"
//#define CTRL_KEY(K) ((K) & 0x1f) 
enum keys{
	arrow_up = 1000,
	arrow_down,
	arrow_right,
	arrow_left,
	page_up,
	page_down,
	home,
	end,
	delete,
	start,
	finish
};
struct editorinfo{
	int cx;
	int cy;
	int screenrows;
	int screencols;
	int rowoff;
	int coloff;
	int numrows;
	int i;
	list l;
	struct termios org;
};
struct editorinfo E;

/***terminal***/
void die(const char *s){
	write(STDOUT_FILENO,"\x1b[2J",4);
	write(STDOUT_FILENO,"\x1b[H",3);
	perror(s);
	exit(1);
}
void disablerow(){
	if(tcsetattr(STDIN_FILENO,TCSAFLUSH,&E.org)==-1)
		die("tcsetattr");
}
void enablerow() {
	if(tcgetattr(STDIN_FILENO, &E.org)==-1)
		die("tcgetattr");
	atexit(disablerow);
	struct termios row=E.org;
	row.c_lflag &= ~(ECHO | ICANON | ISIG |IEXTEN);
	row.c_iflag &= ~(IXON | ICRNL | BRKINT | INPCK | ISTRIP);
	row.c_oflag &= ~(OPOST);
	row.c_cflag |= (CS8);
	row.c_cc[VMIN] = 0;
	row.c_cc[VTIME] = 1;
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &row)==-1)
		die("tcsetattr");
}
int readkey(){
	char c;
	int n;
	
	while(n=read(STDIN_FILENO,&c,1)!=1){
		if(n==-1 && errno != EAGAIN)
			die("read");
	}
	//printf("%d,",c);
	
	if(c == '\x1b') {
		char seq[3];
		if(read(STDIN_FILENO, &seq[0], 1) != 1)
			return '\x1b';
		if(read(STDIN_FILENO, &seq[1], 1) != 1)
			return '\x1b';
		//printf("%d,%d",seq[0],seq[1]);
		if(seq[0] == '[') {
			if(seq[1]>='0' && seq[1]<='9') {
				if(read(STDIN_FILENO, &seq[2], 1) != 1)
					return '\x1b';
				if(seq[2] == '~') {
					switch(seq[1]) {
						case '1':
							return home;
							break;
						case '3':
							return delete;
							break;
						case '4':
							return end;
							break;
						case '5':
							return page_up;
							break;
						case '6':
							return page_down;
							break;
						case '7':
							return home;
							break;
						case '8':
							return end;
							break;
					}
				}
			}
			else {
				switch(seq[1]) {
					case 'A':
						return arrow_up;
						break;
					case 'B':
						return arrow_down;
						break;
					case 'C':
						return arrow_right;
						break;
					case 'D':
						return arrow_left;
						break;
					case 'H':
						return home;
						break;
					case 'F':
						return end;
						break;
				}
			}
		}  
		else if(seq[0]== 'O') {
			if(read(STDIN_FILENO, &seq[1], 1) != 1)
				return '\x1b';
			switch(seq[1]) {
				case 'H':
					return home;
					break;
				case 'F':
					return end;
					break;
			}
		}
			
		return '\x1b';
	}
	else 
		return c;
}
int getwindowsize(int *rows, int *cols) {
  struct winsize ws;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    return -1;
  } else {
    *cols = ws.ws_col;
    *rows = ws.ws_row;
    return 0;
  }
}

/***file io***/
void editoropen(char* filename){
	FILE* fp;
	fp = fopen(filename, "r");
	if(fp == NULL) 
		die("fopen");
	
	char *line = NULL;
	ssize_t linecap = 0;
	ssize_t linelen;
	while((linelen = getline(&line,&linecap,fp)) != -1) {
		while(linelen > 0 && (line[linelen-1] == '\n' || line[linelen-1] == '\r'))
			line[linelen-1] = '\0';
			//linelen--;
		insert(&E.l,line , E.numrows);
		E.numrows++;
	}
	free(line);
	fclose(fp);
}
/***output***/
void scrolling() {
	if(E.cy < E.rowoff) 
		E.rowoff = E.cy;
	if(E.cy >= E.rowoff + E.screenrows)
		E.rowoff = E.cy - E.screenrows +1;
	if(E.cx < E.coloff)
		E.coloff = E.cx;
	if(E.cx >= E.coloff + E.screencols) 
		E.coloff = E.cx - E.screencols + 1;
}
void drawtildes() {
	int i;
	char *r;
	for(i = 0; i < E.screenrows; i++){
		int fileno = i + E.rowoff;
		if(fileno >= E.numrows) {
			if(E.numrows==0 && i == E.screenrows/3) {
				char wel[50];
				int wellen = sprintf(wel, "vn Editor -- version 1.1");
				if(wellen > E.screencols)
					wellen = E.screencols;
				int padding = (E.screencols - wellen) / 2;
				if(padding) {
					write(STDOUT_FILENO, "~", 1);
					padding--;
				}
				while(padding--)
					write(STDOUT_FILENO, " ", 1);
			
				write(STDOUT_FILENO, wel, wellen);
			}
			else {
				write(STDOUT_FILENO, "~",1);
			}
		}
		else {
				r = ctop(&E.l, fileno);
				int len = strlen(r) - E.coloff;
				if(len < 0 )
					len = 0;
				if(len > E.screencols)
					len = E.screencols;
				write(STDOUT_FILENO, r, len);
			
			//int len = E.row[fileno].size - E.coloff;
			//if(len < 0)
			//	len = 0;
			//if(len > E.screencols) 
			//	len = E.screencols;
			//insert(&l, &E.row[fileno].r[E.coloff], len);
			
		}
		write(STDOUT_FILENO, "\x1b[K",3);
		if(i < (E.screenrows - 1))
			write(STDOUT_FILENO,"\r\n",2);
	}
}
void refreashscreen() {
	scrolling();
	write(STDOUT_FILENO, "\x1b[?25l",6);
	write(STDOUT_FILENO,"\x1b[2J",4);
	write(STDOUT_FILENO,"\x1b[H",3);
	drawtildes();
	char buf[32];
	sprintf(buf, "\x1b[%d;%dH",(E.cy - E.rowoff) + 1, (E.cx - E.coloff) + 1);
	//append(&ab, "\x1b[H", 3);
	write(STDOUT_FILENO, buf, strlen(buf));
	write(STDOUT_FILENO, "\x1b[?25h",6);
	
		
}
/***input***/
void movecursor(int key) {
       switch(key) {
		case arrow_left:
			if(E.cx != 0) 
				E.cx--;
			break;
		case arrow_right:
			E.cx++;
			break;
		case  arrow_up:
			if(E.cy != 0)
				E.cy--;
			break;
		case arrow_down:
			if(E.cy < E.numrows)
				E.cy++;
			break; 
		case home:
			E.cx = 0;
			break;
		case end:
			E.cx = (E.screencols - 1);
			break;
		case start:
			E.cx = 0;
			E.cy = 0;
			break;
		case finish:
			E.cx =  (E.screencols - 1);
			E.cy = (E.screenrows - 1);
			break;
	}
}
void processkeypress(){
	int c;
	c=readkey();
	switch(c){
		case 17:/*ctrl q*/
			write(STDOUT_FILENO,"\x1b[2J",4);
			write(STDOUT_FILENO,"\x1b[H",3);
			exit(0);
			break;
		case 14:/*ctrl n */
			movecursor(arrow_down);
			break;
		case 16:/*ctrl p*/
			movecursor(arrow_up);
			break;
		case 6:/*ctrl f*/
			movecursor(arrow_right);
			break;
		case 2:/*ctrl b*/
			movecursor(arrow_left);
			break;
		case page_up:case page_down:
			{
				int time;
				time = E.screenrows;
				while(time--) {
					movecursor(c == page_up ? arrow_up : arrow_down);	
				}
			}
			break;
		case 22:/*ctrl v*/
			{
				int time;
				time = E.screenrows;
				while(time--)
					movecursor(arrow_down);
				
			}
			break;
		case 21:/*ctrl u*/
			{
				int time;
				time = E.screenrows;
				while(time--)
					movecursor(arrow_up);
			}
			break;
		case 1:/*ctrl a*/
			movecursor(start);
			break;
		case 26:/*ctrl z*/
			movecursor(finish);
			break;
		case arrow_up:case arrow_down:case arrow_left:case arrow_right:case home:case end:
			movecursor(c);
			break;
		default:
			break;
			
	}
}
/***init***/
void initEditor() {
	E.cx = 0;
	E.cy = 0;
	E.rowoff = 0;
	E.coloff = 0;
	E.numrows = 0;
	E.i = 0;
	init(&(E.l));
	if(getwindowsize(&E.screenrows,&E.screencols)==-1)
		die("getwindowsize");
}
int main(int agrc, char* agrv[]) {
	enablerow();
	initEditor();
	if(agrc >= 2) {
		editoropen(agrv[1]);
	}
	while(1){
		refreashscreen();
		processkeypress();
	}
	return 0;
}
