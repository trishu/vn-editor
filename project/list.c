#include"list.h"
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#define hl_highlight_numbers (1<<0)
#define hl_highlight_strings (1<<1)
enum highlight {
	hl_normal = 1,
	hl_number,
	hl_search,
	hl_string,
	hl_comment,
	hl_mlcomment,
	hl_keyword1,
	hl_keyword2,
	hl_preprocessor
};
int isseparator(int c) {
	return isspace(c) || c == '\0' || strchr("\\`,()^!#@&$'+-/*|=~%\"<>[]{};?:",c) !=NULL; //not using . as a separator as kdfj787.768
													//should not highlight the no
}
int isnextseparator(int c) {
	return isalpha(c);
}
void init(list *l) {
	l->head = l->tail = NULL;
	l->syntax1 = NULL;
}
void insert(list *l, char* str, int pos) {
	int i, len = strlen(str);
	node *temp, *p;
	p = l->head;
	if(l->head == NULL && pos!= 0)
		return;
	if(pos < 0)
		return;
	for(i = 0; i<(pos - 1); i++) {
		p = p->next;
		if(p == NULL)
			return;
	}
	temp = (node*)malloc(sizeof(node));
	temp->p = (char*)malloc(strlen(str) + 1);
	temp->hl = (char*)malloc((sizeof(char))*(len + 1));
	strcpy(temp->p, str);
	memset(temp->hl, hl_normal, len);
	if(l->syntax1 != NULL) {
		char** keywords1 = l->syntax1->keywords1;
		char** keywords2 = l->syntax1->keywords2;
		char** preprocessor = l->syntax1->preprocessor;
		char *mcs = l->syntax1->ml_comment_start;
		char *mce = l->syntax1->ml_comment_end;
		int mcs_len = mcs ? strlen(mcs) : 0;
  		int mce_len = mce ? strlen(mce) : 0;
	
		int  prev_sep = 1,flag = 0, next_sep, in_string = 0;
		int j = 0, k;
		int in_comment= 0;
		while(j < len) {
			char c = temp->p[j];
			unsigned char prev_hl = (j > 0) ? temp->hl[j - 1] : hl_normal;
			if(c == '/' && temp->p[j + 1] == '/' && !in_string && !in_comment) {
				temp->hl[j] = hl_comment;
				memset(&(temp->hl[j]), hl_comment, len - j);
				break;
			}
			if(mcs_len && mce_len && !in_string) { //actual spanning of multiline comment is remaining
				if(in_comment) {
					temp->hl[j] = hl_mlcomment;
					if(!strncmp(&temp->p[j], mce, mce_len)) {
						memset(&temp->hl[j], hl_mlcomment, mce_len);
						j = j + mce_len;
						in_comment = 0;
						prev_sep = 1;
						continue;
					}
					else {
						j++;
						continue;
					}
				}
				else if(!strncmp(&temp->p[j], mcs, mcs_len)) {
					memset(&temp->hl[j], hl_mlcomment, mcs_len);
					j = j + mcs_len;
					in_comment = 1;
					continue;
				}
			}
			if(l->syntax1->flags & hl_highlight_strings) {
			if(in_string) {
				temp->hl[j] = hl_string;
				if(c == '\\' && (j + 1) < len) {
					temp->hl[j + 1] = hl_string;
					j = j + 2;
					continue;
				}
				if(c == in_string)
					in_string = 0;
				j++;
				prev_sep = 1;
				continue;
			}
			else {
				if(c == '"' || c == '\'' || c== '<') {
					if(c == '<' && temp->p[0] == '#') {
						in_string = '>';
						temp->hl[j] = hl_string;
						j++;
						continue;
					}
					if(c == '"' || c == '\'') {
						in_string = c;
						temp->hl[j] = hl_string;
						j++;
						continue;
					}
				}
			}
			}
			if(l->syntax1->flags & hl_highlight_numbers) {      
			if((isdigit(c) && (prev_sep || prev_hl == hl_number)) || (c == '.' && prev_hl == hl_number)) {
				temp->hl[j] = hl_number;
				j++;
				prev_sep = 0;
				flag = 1;
				continue;
			}
			if(flag == 1){
				next_sep = isnextseparator(c);
				if(next_sep) {
					k = j;
				
					if(temp->hl[k - 1] == hl_number) {
						k = k -1;
						while(temp->hl[k] == hl_number) {
							temp->hl[k] = hl_normal;
							k--;
						}
					}
				}
				flag = 0;
			}
			}
			if(prev_sep) {
				int k = 0, k2 = 0, k3 = 0;
				while(keywords1[k] || keywords2[k2] || preprocessor[k3]){
					if(keywords1[k]) {
						int klen = strlen(keywords1[k]);
					
						if(!strncmp(&temp->p[j], keywords1[k], klen) && isseparator(temp->p[j + klen])) {
							memset(&temp->hl[j], hl_keyword1, klen);
							j = j + klen - 1;
							break;
						}
						k++;
					}
					
					if(keywords2[k2]) {
						int k2len = strlen(keywords2[k2]);
						if(!strncmp(&temp->p[j], keywords2[k2], k2len) && isseparator(temp->p[j + k2len])) {
							memset(&temp->hl[j], hl_keyword2, k2len);
							j = j + k2len - 1;
							break;	
						} 
						k2++;
					}
					if(preprocessor[k3]) {
						int k3len = strlen(preprocessor[k3]);
						if(!strncmp(&temp->p[j], preprocessor[k3], k3len) && isseparator(temp->p[j + k3len])) {
							memset(&temp->hl[j], hl_preprocessor, k3len);
							j = j + k3len - 1;
							break;	
						} 
						k3++;
					}
				}
				if(keywords1[k] == NULL && keywords2[k2] && preprocessor[k3]) {
					prev_sep = 0;
					continue;
				}
			}
			prev_sep = isseparator(c);
			j++;
		}
			
		
	}
	temp->next = NULL;
	if(l->head == NULL) {
		l->head = l->tail = temp;
	}
	else if(pos == 0) {
		temp->next = l->head;
		l->head = temp;
	}
	else if(p->next == NULL) {
		p->next = temp;
		l->tail = temp;
	}
	else {
		temp->next = p->next;
		p->next = temp;
	}
}
char* remov(list *l, int pos) {
	int i;
	char* r;
	node *temp, *p, *q;
	if(l->head == NULL)
		return NULL;
	p = q = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i< pos; i++) {
		q = p;
		p = p->next;
		if(p == NULL)
			return NULL;
	}
	temp =  p;
	r = temp->p;
	if(l->head == NULL) {
		l->head = l->tail = NULL;
	}
	else if(pos == 0) {
		l->head = l->head->next;
		p->next = NULL;
	}
	else if(p->next == NULL) {
		l->tail = q;
		q->next = NULL;
	}
	else {
		q->next = p->next;
		p->next = NULL;
	}
	free(p);
	return r;
}
char* ctop(list *l, int pos) {
	node *p;
	int i;
	p = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i< pos; i++) {
		p = p->next;
		if(p == NULL)
			return NULL;
	}
	return p->p;
}
char *ctopforcolor(list *l, int pos) {
	node *p;
	int i;
	p = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i < pos; i++) {
		p = p->next;
		if(p == NULL)	
			return NULL;
	}
	return p->hl;
} 
char * giverow(list *l, int pos) {
	node *p;
	char *r;
	int i, len;
	p = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i < pos; i++) {
		p = p->next;
		if(p == NULL)
			return NULL;
	}
	r = (char*)malloc(strlen(p->p) + 1);
	strcpy(r, p->p);
	len = strlen(r);
	r[len + 1] = '\0';
	return r;
}
int  listsize(list *l) {
	node *p;
	int len = 0;
	p = l->head;
	if(!p)
		return len;
	while(p) {
		len++;
		p = p->next;
	}
	return len;
}
