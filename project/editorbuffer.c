#include"editorheader.h"
/***includes***/
struct termios org;

enum keys{
	backspace = 127,
	arrow_up = 1000,
	arrow_down,
	arrow_right,
	arrow_left,
	page_up,
	page_down,
	home,
	end,
	delete,
	start,
	finish,
	copy,
	paste
};
/****file type delection***/
char* c_extensions[] = {".c", ".h", ".cpp", NULL};
char* c_keywords1[] = { 
			  "typedef", "struct", "int", "char", "enum","static", "auto", "const", "double", "extern","float","long",
			  "register", "short","signed","union","unsigned", "void", "volatile", NULL
};
char* c_keywords2[] = {
			"if", "while", "switch", "case", "break", "return", "continue", "default", "do", "for",
			"else", "goto", "sizeof", NULL
};
char* c_preprocessor[] = {
			"#define", "#include", "#undef", "#ifdef", "#warning", "#ifndef", "#if", "#else", "#error", "#pragma",
			"#elif", "#endif", "#line", NULL
};
struct syntax highlight[] = { { ".c file", c_extensions, c_keywords1, c_keywords2, c_preprocessor, "/*","*/", hl_highlight_numbers | hl_highlight_strings},};
#define highlight_entries (sizeof(highlight) / sizeof(highlight[0])) 

/***terminal***/
void die(const char *s){
	write(STDOUT_FILENO,"\x1b[2J",4);//clear entire screen 
	write(STDOUT_FILENO,"\x1b[H",3);//reposition the cursor to top left
	perror(s);
	exit(1);
}
void disablerow(){
	if(tcsetattr(STDIN_FILENO,TCSAFLUSH,&org)== -1)
		die("tcsetattr");
}
void enablerow(editor *E) {
	if(tcgetattr(STDIN_FILENO, &org)==-1)
		die("tcgetattr");
	atexit(disablerow);
	struct termios row=org;
	row.c_lflag &= ~(ECHO | ICANON | ISIG |IEXTEN);
	row.c_iflag &= ~(IXON | ICRNL | BRKINT | INPCK | ISTRIP);
	row.c_oflag &= ~(OPOST);
	row.c_cflag |= (CS8);
	row.c_cc[VMIN] = 0;
	row.c_cc[VTIME] = 1;
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &row)==-1)
		die("tcsetattr");
}
int readkey(editor *E){
	char c;
	int n;
	
	while((n=read(STDIN_FILENO,&c,1))!=1){
		if(n==-1 && errno != EAGAIN)
			die("read");
	}
		if(c == '\x1b') {
		char seq[3];
		if(read(STDIN_FILENO, &seq[0], 1) != 1)
			return '\x1b';
		
		
		if(read(STDIN_FILENO, &seq[1], 1) != 1)
			return '\x1b';
		if(seq[0] == '[') {
			if(seq[1]>='0' && seq[1]<='9') {
				if(read(STDIN_FILENO, &seq[2], 1) != 1)
					return '\x1b';
				if(seq[2] == '~') {
					switch(seq[1]) {
						case '1':
							return home;
							break;
						case '3':
							return delete;
							break;
						case '4':
							return end;
							break;
						case '5':
							return page_up;
							break;
						case '6':
							return page_down;
							break;
						case '7':
							return home;
							break;
						case '8':
							return end;
							break;
					}
				}
			}
			else {
				switch(seq[1]) {
					case 'A':
						return arrow_up;
						break;
					case 'B':
						return arrow_down;
						break;
					case 'C':
						return arrow_right;
						break;
					case 'D':
						return arrow_left;
						break;
					case 'H':
						return home;
						break;
					case 'F':
						return end;
						break;
				}
			}
		}  
		else if(seq[0]== 'O') {
			if(read(STDIN_FILENO, &seq[1], 1) != 1)
				return '\x1b';
			switch(seq[1]) {
				case 'H':
					return home;
					break;
				case 'F':
					return end;
					break;
			}
		}
			
		return '\x1b';
	}
	else 
		return c;
}

/***text colouring****/
int syntaxtocolor(editor *E,int hl) {
	switch(hl) {
		case hl_number:
			return 35;//bright mergenta
			break;
		case hl_search:
			return 34;//blue
			break;
		case hl_string:
			return 95;//bright blue
			break;
		case hl_comment:case hl_mlcomment:
			return 36;//cyan
			break;
		case hl_keyword1:
			return 92;//bright green
			break;
		case hl_keyword2:
			return 93;//bright yellow
			break;
		case hl_preprocessor:
			return 96;//bright cyan
			break;
		default:
			return 37;
			break;
	}
}
void selectsyntaxhighlight(editor *E) {
	E->l.syntax1 = NULL;
	if(E->filename == NULL)
		return;
	char* extension = strrchr(E->filename, '.');
	int j;
	for(j = 0; j < highlight_entries; j++) {
		struct syntax* s = &highlight[j];
		int i = 0;
		while(s->filematch[i]) {
			int is_ext = (s->filematch[i][0] == '.');
			if ((is_ext && extension && !strcmp(extension, s->filematch[i])) || 
			(!is_ext && strstr(E->filename, s->filematch[i]))) {
        				E->l.syntax1 = s;
        				return;
      			}
      			i++;
		}
	}
}
/***file io***/
int changecxtorx(editor *E) {
	E->rx = 0;
	int j;
	char *r;
	r = ctop(&(E->l), E->cy_list);
	for(j = 0; j < E->cx; j++) {
		if(r[j] == '\t') 
			E->rx += (7 - (E->rx % 8));
		E->rx++;
	}
	return E->rx;
}
void rowinsertchar(editor *E, char *r,int at, int c) {//insert single character
	int len;
	char a[2];
	len = strlen(r);
	if(at < 0 || at > len)
		at = len;
	r = realloc(r, len + 2);
	memmove(&r[at + 1], &r[at], len - at + 1);
	r[at] = c;
	a[0] = r[at];
	a[1] = '\0';
	
	if(E->undoinsert == 0) {
 		push(&(E->s1), a, E->cy_list, at + 1);
 		E->undoarray[E->undoarrayi] = flags1;
 		E->undoarrayi++;
 		E->undoinsert = 0;
 	}
	insert(&(E->l), r, E->cy_list);
	E->modified++;
}
void rowdeletechar(editor *E, char *r, int at, int rowno) {//delete singal element
	char a[2];
 	int len;
	len = strlen(r);
	if(at < 0 || at >= len)
		return;
	a[0] = r[at];
	a[1] = '\0';
	if(r[at] == '\n') {
		E->numrows--;
	}
	memmove(&r[at], &r[at + 1], len - at);
	if(E->undodelete == 0) {
		push(&(E->s), a, rowno, at);
		E->undoarray[E->undoarrayi] = flagss;
		E->undoarrayi++;
		E->undodelete = 0;
	}
	insert(&(E->l), r, rowno);
	E->modified++;
}
void deleterow(editor *E) {
	char *r;
	if(E->cy_list < E->numrows) {
	r = remov(&(E->l), E->cy_list);
	if(E->undodelete == 0) {
		push(&(E->s), r, E->cy_list, E->cx);
		E->undoarray[E->undoarrayi] = flagss;
		E->undoarrayi++;
		E->undodelete = 0;
	}
	free(r);
	E->numrows--;
	E->modified++;
	}
}
void rowappendstring(editor *E,char *s, int len) {
	char *r;
	char a[2];
	a[0] = '`';
	a[1] = '\0';
	int size;
	r = remov(&(E->l), E->cy_list -1);
	size = strlen(r);
	if(E->fromdelete == 1 && E->undodelete == 0) {
		push(&(E->s), a, E->cy_list - 1, size);
		E->undoarray[E->undoarrayi] = flagss;
		E->undoarrayi++;
		E->fromdelete = 0;
		E->undodelete = 0;
	}
	E->cx = size;
	r = realloc(r, size + len + 1);
	memcpy(&r[size], s, len);
	size = size + len;
	r[size] = '\0';
	insert(&(E->l), r, E->cy_list - 1);
	E->modified++;	
}
int editoropen(editor *E,char* filename) {
	if(E->filename)
		free(E->filename);
	E->filename = strdup(filename);
	selectsyntaxhighlight(E);
	FILE* fp;
	fp = fopen(filename, "r");
	if(fp == NULL) 
		return -1;
//		die("fopen");
	int i = 0;
	char *line = NULL;
	size_t linecap = 0;
	ssize_t linelen;
	while((linelen = getline(&line,&linecap,fp)) != -1) {
		while(linelen > 0 && (line[linelen-1] == '\n' || line[linelen-1] == '\r'))
			line[linelen-1] = '\0';
		//	linelen--;
		insert(&(E->l),line , E->numrows);
		E->numrows++;
	}
	free(line);
	while(i < E->numrows) {
		char *r,*p, *m;
		int l, l1, j, k = 0, tab = 0, t = 0;
		r = ctop(&(E->l), i);
		if(r!= NULL) {
		l = strlen(r);
		for(j = 0; j< l; j++) {
			if(r[j]=='\t')
				tab++;
		}
		if(tab > 0) {
			m = remov(&(E->l), i);
			l1 = l + 7 * tab;
			p = (char*)malloc(l1 + 1);
			for(j = 0; j < l; j++) {
				if(m[j]== '\t') {
					while(t<8) {
						p[k]=' ';
						k++;
						t++;
					}
					t = 0;
				}
				else{
					p[k] = m[j];
					k++;
				}
			}
			p[k] = '\0';
			free(m);
			insert(&(E->l), p, i);
		}
		i++;
		}
	}
	fclose(fp);
	E->modified = 0;
	return 0;
}
void makefilecolorful(editor *E) {
	char *p;
	int i;
	for(i = 0; i < E->numrows; i++) {
		p = remov(&(E->l), i);
		insert(&(E->l), p, i);
	}
}
void savefile(editor *E) {
	if(E->filename == NULL) {
		E->filename = editorPrompt(E , "Save as : %s", NULL);
	
	    if (E->filename == NULL) {
		      setstatusmessage(E,"Save aborted");
	      return;
	    }
	    selectsyntaxhighlight(E);
	}

	FILE *fp;
	fp = fopen(E->filename, "w");
	if(!fp) {
		setstatusmessage(E, "Can't save due to some I/O problem : %s", strerror(errno));
		return;
	}
	int i;
	char *r;
	for(i = 0; i < E->numrows; i++) {
		r = ctop(&(E->l), i);
		fprintf(fp, "%s", r);
		fprintf(fp, "\n");
	}
	E->modified = 0;
	setstatusmessage(E,"%s file is saved on disk!!", E->filename);
		makefilecolorful(E);    
	fclose(fp);
}
/***search***/
void searchlinesstore(editor *E) {
	int i;
	char *p, *find;
	if(E->searchlines)
		free(E->searchlines);
	E->searchlines = (int*)malloc(sizeof(int) * E->searchsize);
	E->sn = 0;
	for(i = 0; i < E->numrows; i++) {
		p = ctop(&(E->l),i);
		find = strstr(p, E->findout);
		if(find) {
			if(E->sn == E->searchsize) {
				E->searchlines = realloc(E->searchlines,(sizeof(int)) *(E->searchsize) * 2);
				E->searchsize = E->searchsize * 2;
			}
			E->searchlines[E->sn] = i;
			E->sn++;
		}
	}
}
void findcontinuously(editor *E, char *findout, int key){
	static int lastmatch = -1;//-1 means no last match or it contain the rownumber where we have prevmatch
	static int direction = 1;//1 search forward & -1 search backward
	static int saved_hl_line;
 	static char* saved_hl = NULL;
	
	if(saved_hl) {
		char* q = ctopforcolor(&(E->l), saved_hl_line);
		memcpy(q, saved_hl, strlen(q));
		free(saved_hl);
		saved_hl = NULL;
	}	
	if(key == '\r' || key == '\x1b') {
		lastmatch = -1;
		direction = 1;
		return;
	}
	else if(key == arrow_right || key == arrow_down)
		direction = 1;
	else if(key == arrow_left || key == arrow_up)
		direction = -1;
	else {
		lastmatch = -1;
		direction = 1;
	}
	if(lastmatch == -1)
		direction = 1;
	int current = lastmatch;
	char *r, *ans, *m;
	int i;
	for(i = 0; i < E->numrows; i++) {
		current = current + direction;
		if(current == -1)
			current = E->numrows - 1;
		else if(current == E->numrows)
			current = 0;
		r = ctop(&(E->l), current);
		m =r;
		ans = strstr(r, findout);
		if(ans) {
			lastmatch = current;
			E->cy_list = current;
			E->cy = current + E->printstart;
			E->cx = ans - r;
			E->rowoff = E->numrows;
			char* p = ctopforcolor(&(E->l), current);
			saved_hl_line = current;
			saved_hl = malloc(strlen(m));
			memcpy(saved_hl, p, strlen(p));
			memset(&p[ans - r],hl_search,strlen(findout));
			break;
		}
	}
}
void find(editor *E) {
	int prev_cx = E->cx;
	int prev_cy = E->cy_list;
	int prev_rowoff = E->rowoff;
	int prev_coloff = E->coloff;
	if(E->findout) {
		free(E->findout);
		E->findout = NULL;
	}
	E->findout = editorPrompt(E, "Search: %s", findcontinuously);
	if(!E->findout){
		E->cx = prev_cx;
		E->cy_list = prev_cy;
		E->cy = E->cy_list + E->printstart;
		E->rowoff = prev_rowoff;
		E->coloff = prev_coloff;
	}
}
void findline(editor *E) {
	int linenumber;
	char* lineno = editorPrompt(E, "enter line no: %s", NULL);
	if(isdigit(lineno[0])) {
		linenumber = atoi(lineno);
		if(linenumber <= E->numrows && linenumber > 0) {
			E->cy = linenumber - 1;
			E->cy = E->cy + E->printstart;
			E->cy_list = E->cy - E->printstart;
			E->cx = 0;
		}
		else {
			setstatusmessage(E, "invalid linenumber");
		}
	}
	else {
		setstatusmessage(E, "invalid linenumber");
	}
}
int stringreplace(editor *E, char *text, char *orig, char *new) {
	char *p;
	char mod[1000] = "";
	memset(mod, '\0', strlen(mod));
	int r = 0,z, l, i , j, k = 0, m, q, len,n;
	len = strlen(orig);
	while((p = strstr(text, orig)) != NULL) {
		l = p - text;
		m = l;
		z = l + len;
		if(l != 0)
			n = l - 1;
		else 
			n = z;
		if((!isalnum(text[z])) && !isalnum(text[n])) {
		r++;
		for(i = 0, q = k; i < l; i++, q++) {
			mod[q] = text[i];
		}
		mod[q] = '\0';
		strcat(mod, new);
		k = strlen(mod);
		}
		else {
			for(i = 0, q = k; i < z; i++,q++)
				mod[q] = text[i];
			mod[q] = '\0';
			k = strlen(mod);
		}
		for(j = 0; text[j] != '\0'; j++, m++)
			text[j] = text[m + strlen(orig)];
		
		
	}

		strcat(mod, text);
		strcpy(text, mod);
	
	return r;
}
void replace(editor *E) {
	if(E->replace)
		free(E->replace);
	E->replace = editorPrompt(E,"replace with: %s", NULL);
	if(!(E->replace))
		return;
	int i;
	int countinline = 0, countinfile = 0;
	char *r;
	for(i = 0; i < E->sn; i++) {
		r = remov(&(E->l), E->searchlines[i]);
		countinline = stringreplace(E, r, E->findout, E->replace);
		countinfile += countinline;
		insert(&(E->l), r, E->searchlines[i]);
	}
	E->modified++;
	char str[100];
	if(countinfile)
		sprintf(str, "%d replaced done!!!", countinfile);
	else
		sprintf(str, "\"%s\" is not present in file as a word", E->findout);
	if(E->findout) {
		free(E->findout);
		E->findout = NULL;
	}
	setstatusmessage(E, str);
}
int stringdelete(editor *E,char *text, char *orig) {
	char *p;
	char mod[1000] = "";
	memset(mod, '\0', strlen(mod));
	int r = 0,z, l, i , j, k = 0, m, q, len,n;
	len = strlen(orig);
	while((p = strstr(text, orig)) != NULL) {
		l = p - text;
		m = l;
		z = l + len;
		printf("vijay\n");
		printf("%d, %d\n", l, z);
		if(l != 0)
			n = l - 1;
		else 
			n = z;
		if((!isalnum(text[z])) && !isalnum(text[n])) {
			r++;
			printf("mahi\n");
			for(i = 0, q = k; i < l; i++, q++) {
				mod[q] = text[i];
			}
			mod[q] = '\0';
			k = strlen(mod);
		}
		else {
			for(i = 0, q = k; i < z; i++,q++)
				mod[q] = text[i];
			mod[q] = '\0';
			k = strlen(mod);
		}
		for(j = 0; text[j] != '\0'; j++, m++)
			text[j] = text[m + strlen(orig)];
		
		
	}

		strcat(mod, text);
		strcpy(text, mod);
	
	return r;
}
void searchanddelete(editor *E) {
	int i, countinline, countinfile = 0;
	char *r;
	for(i = 0; i < E->sn; i++) {
		r = remov(&(E->l), E->searchlines[i]);
		countinline = stringdelete(E, r, E->findout);
		countinfile +=countinline;
		insert(&(E->l), r, E->searchlines[i]);
	}
	E->modified++;
	char str[1000];
	if(countinfile)
		sprintf(str, "%d deletion done!!! \"%s\" is deleted from file", countinfile, E->findout);
	else
		sprintf(str, "\"%s\" is not in file as a word", E->findout);
	if(E->findout) {
		free(E->findout);
		E->findout = NULL;
	}
	setstatusmessage(E,str);
}
/***editor operations***/
void insertchar(editor *E, int c) {
	if(E->cy_list == E->numrows) {
		insert(&(E->l), "", E->numrows);
		E->numrows++;
	}
	char *r;
	r = remov(&(E->l), E->cy_list);
	rowinsertchar(E, r, E->cx, c);
	E->cx++;	
}
void undonewline(editor *E) {
	char a[2];
	a[0] = '\n';
	a[1] = '\0';
	push(&(E->s1), a, E->cy_list, E->cx);
	E->undoarray[E->undoarrayi] = flags1;
	E->undoarrayi++;
}
void insertnewlineforundo(editor *E) {
	if(E->cx == 0) 
		insert(&E->l, "", E->cy_list);
	else {
		char *r;
		int len;
		r = remov(&E->l, E->cy_list);
		insert(&E->l, &r[E->cx], E->cy_list);
		len = E->cx;
		r[len] = '\0';
		insert(&E->l , r, E->cy_list);
	}
	E->cy_list++;
	E->cy = E->cy_list + E->printstart;
	E->cx = 0;
}
void insertnewline(editor *E) {
	int i = 0, len, j;
	char *r;
	char str[1000] = "", a[2] = "";
	if(E->cx == 0) {
		insert( &(E->l), "", E->cy_list);
	}

	else {
		
		memset(str, '\0', strlen(str));
		i = 0;
		r = remov(&(E->l), E->cy_list);
		while(r[i] == '\t' || r[i] == ' ') {
			if(r[i] == '\t') {
				str[i] = '\t';
				a[0] = r[i];
				a[1] = '\0';
				push(&(E->s1), a, E->cy_list + 1, i);
				E->undoarray[E->undoarrayi] = flags1;
				E->undoarrayi++;	
				i++;
			}
			else if(r[i] == ' ') {
				str[i] = ' ';
				a[0] = r[i];
				a[1] = '\0';
				
				push(&(E->s1), a, E->cy_list + 1, i);
				E->undoarray[E->undoarrayi] = flags1;
				E->undoarrayi++;
				i++;
			}
		}
		str[i] = '\0';
		j = E->cx;
		while(r[j] == ' ' || r[j]=='\t') {
			j++;
		}
		strcat(str, &r[j]);
		insert(&(E->l), str, E->cy_list);
		len = E->cx;
		r[len] = '\0';
		insert(&(E->l) , r, E->cy_list);
	}
	E->numrows++;
	E->cy++;
	E->cy_list++;
	E->cx = i;
}

void deletechar(editor *E) {
	char *r;
	if(E->cy_list == E->numrows)
		return;
	if(E->cx == 0 && E->cy_list == 0)
		return;
	
	if(E->cx > 0) {
		r = remov(&(E->l), E->cy_list);
		rowdeletechar(E, r, E->cx - 1, E->cy_list);
		E->cx--;
	}
	else {
		char *r;
		int len;
		r = remov(&(E->l), E->cy_list);
		len = strlen(r);
		rowappendstring(E, r, len);
		E->numrows--;
		E->cy--;
		E->cy_list--;
	}
	
}

void cuttextforundo(editor *E, int cutstarty, int cutstartx, int cutendy, int cutendx) {
	int j = cutstarty, len;
	char *r, *p;
	char str[1000] = "", str1[1000];
	if(cutstarty == cutendy) {
		r = remov(&(E->l), cutstarty);
		strcpy(str1, &r[cutendx]);
		r[cutendx] = '\0';
		strcpy(str, &r[cutstartx]);
		r[cutstartx] = '\0';
		strcat(r, str1);
		insert(&(E->l), r, cutstarty);
		
	}
	else {
		r = remov(&(E->l), cutstarty);
		strcpy(str, &r[cutstartx]);
		r[cutstartx] = '\0';
		j++;
		while(j < cutendy) {
			p = remov(&(E->l), cutstarty);
			j++;
			E->numrows--;
		}
		p = remov(&(E->l), cutstarty);
		strcpy(str1, &p[cutendx]);
		p[cutendx] = '\0';
		E->numrows--;
		r = realloc(r, strlen(r)+ strlen(str1) + 1);
		strcat(r, str1);
		len = strlen(r);
		r[len + 1] = '\0';
		insert(&(E->l), r, cutstarty);
	}
	E->cy = cutstarty + E->printstart;
	E->cy_list = cutstarty;
	E->cx = cutstartx;
	E->modified++;
}

void undo(editor *E) {
	char *r;
	int len, pasty, pastx, cstarty, cstartx, cendy, cendx;
	int c;
	data d;
	E->undoarrayi--;
	if(E->undoarrayi <= -1) {
		E->undoarrayi = 0;
		return;
	}
	switch(E->undoarray[E->undoarrayi]) {
		case flagss:
			if(!isempty(&(E->s))) {
			static int prevrow = INT_MIN ;
			static int prevcol = INT_MIN; 
				E->undoinsert = 1;
				r = pop(&(E->s), &(E->cy_list), &(E->cx));
				E->cy = E->cy_list + E->printstart;
				len = strlen(r);
				if(len == 1) {				//insert a single deleted char
					c = r[0];
					if(E->cx != 0) {
						prevrow = E->cy_list;
						prevcol = E->cx;
						
						if(c == '`')
							insertnewlineforundo(E);
						else
							insertchar(E, c);
					}
					else if(E->cx == 0 && prevrow == E->cy_list - 1 && prevcol != E->cx){
						char *r1;
						char str[1000] = "", str1[1000] = "";
						str[0] = r[0];
						if(c == '`')
							insertnewlineforundo(E);
						else {
						r1 = remov(&(E->l), E->cy_list - 1);
						strcpy(str1, &r1[prevcol + 1]);
						strcat(str, str1);
						r1[prevcol + 1] = '\0';
						insert(&(E->l), r1, E->cy_list - 1);
						insert(&(E->l), str, E->cy_list );
						}
						prevrow = INT_MAX;
						prevcol = INT_MAX;
					}
					else {
						if(c == '`')
							insertnewlineforundo(E);
						else
							insertchar(E, c);
					}
							
				}
				else  {
					insert(&(E->l), r, E->cy_list); 			//insert a previous deleted row
					E->numrows++;
				}
			}
			break;
		case flags1:
				if(!isempty(&(E->s1))) {
				r = pop(&(E->s1), &(E->cy_list) , &(E->cx));
				E->cy = E->cy_list + E->printstart;
				len = strlen(r);
				if(len == 1) {
						c = r[0];
						E->undodelete = 1;
						deletechar(E);
				}			//delete a previous inserted singal char
				
				else {
					E->undodelete = 1;
					deleterow(E);			//delete previous inserted row
			
				}
			}
			break;
		case flags3:
				if(!uisempty(&(E->s3))) {			//undo for cut and paste part
				d = upop(&(E->s3));
				if(d.pastey == INT_MIN) {
					pasty = d.cutstarty;
					pastx = d.cutstartx;
					E->frompaste = 0;
					pastetext(E,pasty, pastx, d.l);
				}
				else if(d.cutstarty == INT_MIN) {
					cstarty = d.pastey;
					cstartx = d.pastex;
					cendy = d.cutendy;
					cendx = d.cutendx;
					cuttextforundo(E, cstarty, cstartx, cendy, cendx);
				}
			}
			break;
		default:
			break;
	}
}
void copyline(editor *E) {
	if(E->copyline) {
		free(E->copyline);
		E->copyline = NULL;
	}
	if(E->cy_list < E->numrows) {
	char *r;
	r = ctop(&(E->l), E->cy_list);
	if(r != NULL) {
	E->copyline = (char*)malloc(strlen(r) + 1);
	strcpy(E->copyline, r);
	}
	}
}
void pasteline(editor *E) {
	if(E->copyline) {
	insert(&(E->l), E->copyline, E->cy_list);
	push(&(E->s1), E->copyline, E->cy_list, E->cx);
	E->undoarray[E->undoarrayi] = flags1;
	E->undoarrayi++;
	E->numrows++;
	E->modified++;
	}
}
void copystart(editor *E) {
	init(&(E->buffl));
	E->buffrow = 0;
	E->copybeginy = E->cy_list;
	E->copybeginx = E->cx;
}
void copyend(editor *E) {
	E->copyendy = E->cy_list;
	E->copyendx = E->cx;
}
void cutstart(editor *E) {
	init(&(E->buffl));
	E->buffrow  = 0;
	E->cutstarty = E->cy_list;
	E->cutstartx = E->cx;
}
void cutend(editor *E) {
	E->cutendy = E->cy_list;
	E->cutendx = E->cx;
}
void pasteposition(editor *E) {
	E->pastex = E->cx;
	E->pastey = E->cy_list;
}
void copytext(editor *E) {
	char *r;
	char str[1000] = "";
	int n;
	if(E->copyendy >= E->numrows) {
		setstatusmessage(E, "invalid copy end position");
		return;
	}
	if(E->copybeginy > E->copyendy) {
		setstatusmessage(E,"invalid copy start and end position");
		return;
	}
	if(E->copybeginy == E->copyendy && E->copybeginx >= E->copyendx) {
		setstatusmessage(E,"invalid copy start and end position");
		return;
	}	
	if(E->copybeginy==E->copyendy) {
		r = ctop(&(E->l), E->copybeginy);
		n = E->copyendx - E->copybeginx;
		strncpy(str, &r[E->copybeginx], n);
		insert(&(E->buffl), str, E->buffrow);
		E->buffrow++;
	}
	else {
		r = ctop(&(E->l), E->copybeginy);
		strcpy(str, &r[E->copybeginx]);
		insert(&(E->buffl), str, E->buffrow);
		E->buffrow++;
		E->copybeginy++;
		memset(str, '\0', strlen(str));
		while(E->copybeginy < E->copyendy) {
			
			r = ctop(&(E->l), E->copybeginy);
			strcpy(str, r);
			insert(&(E->buffl), str, E->buffrow);
			E->buffrow++;
			E->copybeginy++;
			memset(str, '\0', strlen(str));
		}
		r = ctop(&(E->l), E->copybeginy);
		strncpy(str, r, E->copyendx);
		insert(&(E->buffl), str, E->buffrow);
		E->buffrow++;
	}
	
}

void cuttext(editor *E) {
	E->buffrow = 0;
	data d;
	int j = E->cutstarty, len;
	char *r, *p;
	char str[1000] = "", str1[1000];
	if(E->cutendy >= E->numrows) {
		setstatusmessage(E, "invalid cut end position");
		return;
	}
	if(E->cutstarty > E->cutendy) {
		setstatusmessage(E,"invalid cut start and end position");
		return;
	}
	if(E->cutstarty == E->cutendy && E->cutstartx >= E->cutendx) {
		setstatusmessage(E,"invalid cut start and end position");
		return;
	}
	if(E->cutstarty == E->cutendy) {
		r = remov(&(E->l), E->cutstarty);
		strcpy(str1, &r[E->cutendx]);
		r[E->cutendx] = '\0';
		strcpy(str, &r[E->cutstartx]);
		r[E->cutstartx] = '\0';
		strcat(r, str1);
		insert(&(E->l), r, E->cutstarty);
		insert(&(E->buffl), str, E->buffrow);
		E->buffrow++;
		
	}
	else {
		r = remov(&(E->l), E->cutstarty);
		strcpy(str, &r[E->cutstartx]);
		r[E->cutstartx] = '\0';
		insert(&(E->buffl), str, E->buffrow);
		j++;
		E->buffrow++;
		while(j < E->cutendy) {
			p = remov(&(E->l), E->cutstarty);
			insert(&(E->buffl), p, E->buffrow);
			j++;
			E->buffrow++;
			E->numrows--;
		}
		p = remov(&(E->l), E->cutstarty);
		strcpy(str1, &p[E->cutendx]);
		p[E->cutendx] = '\0';
		insert(&(E->buffl), p, E->buffrow);
		E->buffrow++;
		E->numrows--;
		r = realloc(r, strlen(r)+ strlen(str1) + 1);
		strcat(r, str1);
		len = strlen(r);
		r[len + 1] = '\0';
		insert(&(E->l), r, E->cutstarty);
	}
	E->cy = E->cutstarty + E->printstart;
	E->cy_list = E->cutstarty;
	E->cx = E->cutstartx;
	E->modified++;
	d.cutstarty = E->cutstarty;
	d.cutstartx = E->cutstartx;
	d.cutendy = E->cutendy;
	d.cutendx = E->cutendx;
	d.pastey = INT_MIN;
	d.pastex = INT_MIN;
	d.l = &(E->buffl);
	upush(&(E->s3), d);
	E->undoarray[E->undoarrayi] = flags3;
	E->undoarrayi++;
}
void pastetext(editor *E,int pastey, int pastex, list *b) {
	int i = 0, len, py = pastey, px = pastex, cutx;
	char str[1000] = "";
	char *r, *p;
	data d;
	if(E->pastey >= E->numrows) {
		setstatusmessage(E,"invalid paste position");	
		return;
	}
	int buffrow = listsize(b);
	if(buffrow != 0) {
	if(buffrow == 1) {
		r = remov(&(E->l), py);
		strcpy(str, &r[px]);
		r[px] = '\0';
		p = giverow(b, i);
		i++;
		r = realloc(r, strlen(r) + strlen(p) + 1);
		strcat(r, p);
		len = strlen(r);
		cutx = len;
		r[len + 1] = '\0';
		r = realloc(r, strlen(r) + strlen(str) + 1);
		strcat(r, str);
		len = strlen(r);
		r[len + 1]= '\0';
		insert(&(E->l), r, py);
	}
	else {
		
		r = remov(&(E->l), py);
		strcpy(str, &r[px]);
		r[px] = '\0';
		p = giverow(b, i);
		i++;
		r = realloc(r, strlen(r) + strlen(p) + 1);
		strcat(r, p);
		len = strlen(r);
		r[len + 1] = '\0';
		insert(&(E->l), r, py);
		py++;
		while(i < (buffrow - 1)) {
			p = giverow(b, i);
			i++;
			insert(&(E->l), p, py);
			py++;
			E->numrows++;
		}
		p = giverow(b, i);
		p = realloc(p, strlen(p) + 1);
		len = strlen(p);
		cutx = len; 
		p[len + 1]= '\0';
		p = realloc(p, strlen(p) + strlen(str) + 1);
	
		strcat(p, str);
		len = strlen(p);
		p[len + 1] = '\0';
		insert(&(E->l), p, py);
		E->numrows++;
		
	}
	E->modified++;	
	if(E->frompaste == 1) {
		d.cutstarty = INT_MIN;
		d.cutstartx = INT_MIN;
		d.cutendy = py;
		d.cutendx = cutx;
		d.pastey = pastey;
		d.pastex = pastex;
		d.l = b;
		upush(&(E->s3), d);
		E->undoarray[E->undoarrayi] = flags3;
		E->undoarrayi++;
		E->frompaste = 0;
	}
	}
}
/***output***/
void scrolling(editor *E) {
	E->rx = 0;
	if(E->cy_list < E->numrows) {
		E->rx = changecxtorx(E);
	}
	if(E->cy_list < E->rowoff) 
		E->rowoff = E->cy_list;
	if(E->cy_list >= E->rowoff + E->screenrows)
		E->rowoff = E->cy_list- E->screenrows +1;
	if(E->rx < E->coloff)
		E->coloff = E->rx;
	if(E->rx >= E->coloff + E->screencols) 
		E->coloff = E->rx - E->screencols + 1;
}
void drawtildes(editor *E) {
	char *r;
	for(E->i = 0; E->i < E->screenrows; E->i++) {
		int fileno = E->i + E->rowoff;
		
		if(fileno >= E->numrows) {
			if(E->numrows==0 && E->i == E->screenrows/3) {
				char wel[50];
				int wellen = sprintf(wel, "vn Editor -- version 1.1");
				if(wellen > E->screencols)
					wellen = E->screencols;
				int padding = (E->screencols - wellen) / 2;
				if(padding) {
					write(STDOUT_FILENO, "~", 1);
					padding--;
				}
				while(padding--)
					write(STDOUT_FILENO, " ", 1);
			
				write(STDOUT_FILENO, wel, wellen);
			}
			else {
				write(STDOUT_FILENO, "~",1);
			}
			write(STDOUT_FILENO, "\x1b[K",3);//erases a part of line to right of cursor
		write(STDOUT_FILENO,"\r\n",2);
		}
		else {
				r = ctop(&(E->l), fileno);
				int len = strlen(r) - E->coloff;
				if(len < 0 )
					len = 0;
				if(len > E->screencols) {
					len = E->screencols;
				}
				char* hl = ctopforcolor(&(E->l), fileno);
				int current_color = -1;
				int j;
				for(j = E->coloff; j < (len + E->coloff); j++) {
					if(hl[j] == hl_normal) {
						if(current_color != -1) {
							write(STDOUT_FILENO, "\x1b[0;39m",7);
							current_color = -1;
						}
						write(STDOUT_FILENO, &r[j], 1);
					}
					else {
						int color = syntaxtocolor(E, hl[j]);
						if(color != current_color) {
							current_color = color;
							char buf[32];
							int u = 0 ;
							if(color == 35)
								u = 1;
							else 
								u = 0;
							int clen = snprintf(buf, sizeof(buf),"\x1b[%d;%dm",u,color);
							write(STDOUT_FILENO, buf,clen);
						}
						write(STDOUT_FILENO, &r[j], 1);
					}
				}
				write(STDOUT_FILENO, "\x1b[0;39m", 7);
				write(STDOUT_FILENO, "\x1b[K",3);//erases a part of line to right of cursor
				write(STDOUT_FILENO,"\r\n",2);	
		
		}
}
}
void statusbar(editor *E) {
	write(STDOUT_FILENO , "\x1b[1;7m", 6);
	char status[100], rstatus[100];
	int len = snprintf(status, sizeof(status), "%s - %d lines %s",E->filename ? E->filename : "[untitled file]", E->numrows,
	 E->modified?"[modified]":"");
	int rlen = snprintf(rstatus, sizeof(rstatus), "%s |  %d/%d",E->l.syntax1 ? (E->l.syntax1)->filetype:"no filetype",
	(E->cy - E->printstart) + 1, E->numrows);
  
	if(len > E->screencols)
		len = E->screencols;
	write(STDOUT_FILENO, status, len);
	while(len < E->screencols) {
		if (E->screencols - len == rlen) {
                	write(STDOUT_FILENO, rstatus, rlen);
                	break;
                }
                else {
			write(STDOUT_FILENO, " ", 1);
			len++;
		}
	}
	
	write(STDOUT_FILENO, "\x1b[m", 3);
	write(STDOUT_FILENO, "\r\n", 2);
	
}
void DrawMessageBar(editor *E) {
  write(STDOUT_FILENO, "\x1b[K", 3);
  int msglen = strlen(E->statusmsg);
  if (msglen > E->screencols) 
  msglen = E->screencols;
  if (msglen && time(NULL) - E->statusmsg_time < 5)
    write(STDOUT_FILENO, E->statusmsg, msglen);
}
void refreashscreenfor2buffer(editor *E) {
	scrolling(E);
	char currep[32];
	sprintf(currep, "\x1b[%d;%dH", E->printstart + 1, 1);
	write(STDOUT_FILENO, "\x1b[?25l", 6);
	write(STDOUT_FILENO, currep, strlen(currep));
	write(STDOUT_FILENO, "\x1b[0J", 4);
	write(STDOUT_FILENO, currep, strlen(currep));
	drawtildes(E);
	statusbar(E);
	DrawMessageBar(E);
}
void showcursor(editor *E) {
	write(STDOUT_FILENO, "\x1b[?25l",6);//hide the cursor
	char buf[32];
	sprintf(buf, "\x1b[%d;%dH",(E->cy - E->rowoff) + 1, (E->rx - E->coloff) + 1);
	write(STDOUT_FILENO, buf, strlen(buf));
	write(STDOUT_FILENO, "\x1b[?25h",6);//show the cursor
	
}
void refreashscreen(editor *E) {
	scrolling(E);
	char currep[32];
	sprintf(currep, "\x1b[%d;%dH", E->printstart + 1, 1);
	write(STDOUT_FILENO, "\x1b[?25l",6);//hide the cursor
	write(STDOUT_FILENO,currep,strlen(currep));
	write(STDOUT_FILENO,"\x1b[0J",4);//clear screen from cursor to end of screen 
	write(STDOUT_FILENO,currep,strlen(currep));//reposition the cursor to top left
	drawtildes(E);
	statusbar(E);
	DrawMessageBar(E);
	char buf[32];
	sprintf(buf, "\x1b[%d;%dH",(E->cy - E->rowoff) + 1, (E->rx - E->coloff) + 1);
	write(STDOUT_FILENO, buf, strlen(buf));
	write(STDOUT_FILENO, "\x1b[?25h",6);//show the cursor
	
		
}
void setstatusmessage(editor *E,const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(E->statusmsg, sizeof(E->statusmsg), fmt, ap);
  va_end(ap);
  E->statusmsg_time = time(NULL);
}

/***input***/
char* editorPrompt(editor *E,char *prompt, void (*callback)(editor*, char *, int)) {
		
	  size_t bufsize = 128;
  char *buf = malloc(bufsize);
  size_t buflen = 0;
  buf[0] = '\0';
  while (1) {
    setstatusmessage(E,prompt, buf);
	refreashscreen(E);
    int c = readkey(E);
    if (c == delete || c == CTRL_KEY('h') || c == backspace) {
      if (buflen != 0) buf[--buflen] = '\0';
    } 
    else if(c == '\x1b') {
    	setstatusmessage(E,"");
    	if (callback) callback(E, buf, c);
    		free(buf);
    	return NULL;
    }
   else if (c == '\r') {
      if (buflen != 0) {
        setstatusmessage(E,"");
        if (callback) callback(E,buf, c);
        return buf;
      }
    }
     else if (!iscntrl(c) && c < 128) {
      if (buflen == bufsize - 1) {
        bufsize *= 2;
        buf = realloc(buf, bufsize);
      }
      buf[buflen++] = c;
      buf[buflen] = '\0';
    }
    if (callback) callback(E,buf, c);
  }
}
void moveforwardbyword(editor *E) {
	char *r;
	int  j, len;
	if(E->cy_list < E->numrows ) {
		r = ctop(&E->l, E->cy_list);
		len = strlen(r);
		if(E->cx == len && E->cy_list != (E->numrows - 1)) {
			E->cy = E->cy + 1;
			E->cy_list = E->cy_list + 1;
			r = ctop(&(E->l), E->cy_list);
			j = 0;
			while(isspace(r[j])) {
				j++;
			}
			E->cx = j;
			return;
		}
		j = E->cx;
		while(j <= len) {
			 if(r[j] == '\0') {
				E->cx = j;
				if(E->cy_list == (E->numrows - 1))
					return;
				E->cy = E->cy + 1;
				E->cy_list = E->cy_list + 1;
				r = ctop(&(E->l), E->cy_list);
				j = 0;
				while(isspace(r[j]))
					j++;
				E->cx = j;
				break;
				
			}
			else if(r[j] == ' ' && r[j+1]==' ') {
				while(isspace(r[j]))
					j++;
				E->cx = j;
				break;
			}
			else if(r[j] == ' ') {
				E->cx = j + 1;
				break;
			}
			j++;
		}
	}
}
void movebackwardbyword(editor *E) {
	char *r;
	int len, j;
	static int flag;
	if(E->cy_list >= 0 && E->cy_list < E->numrows) {
		r = ctop(&(E->l), E->cy_list);
		len = strlen(r);
		
		if((E->cx == 0 && E->cy_list != 0) || (r[E->cx]=='\t' && E->cx==0) || flag == 1) {
			flag = 0;
			E->cy = E->cy - 1;
			E->cy_list = E->cy_list - 1;
			r = ctop(&(E->l), E->cy_list);
			len = strlen(r);
			E->cx = len;
		}
		j = E->cx;
		if(r[j] == ' ' || r[j] == '\t') {
			j = j - 1;
			while((isspace(r[j]) || r[j] == '\t') && j!=0)
				j--;
			while(!isspace(r[j]) && r[j] != '\t' && j!=0)
				j--;
			if(j == 0)  {
				if(r[j] == ' ') {
					E->cx = 0;
					movebackwardbyword(E);
				}
				else if(r[j] == '\t') {
					flag = 1;
					while(r[j] == '\t')
						j++;
					E->cx = j;
				}
				else
					E->cx = 0;
			}
			else
				E->cx = j + 1;
		}
		else if(r[j - 1] == ' ' || r[j - 1] == '\t') {
			j = j - 1;
			if(r[j] == '\t' && (j) == 0){
				E->cx = 0;
				movebackwardbyword(E);
			}
				
			while((isspace(r[j]) || r[j] == '\t') && j!=0)
				j--;
			while(!isspace(r[j]) && r[j]!= '\t' && j!= 0)
				j--;
			if(j == 0) {
				if(r[j] == ' ') {
					E->cx = 0;
					movebackwardbyword(E);
				}
				else if(r[j] == '\t') {
					flag = 1;
					while(r[j] == '\t')
						j++;
					E->cx = j;
				}
				else
					E->cx = 0;
			}
			else 
				E->cx = j + 1;
		}
		else {
			while((isspace(r[j]) || r[j] == '\t') && j!=0)
				j--;
			while(!isspace(r[j]) && r[j]!='\t' && j!=0)
				j--;
			if(j == 0) {
				if(r[j] == ' ') {
					E->cx = 0;
					movebackwardbyword(E);
				}
				else if(r[j] == '\t') {
					flag = 1;
					while(r[j] =='\t')
						j++;
					E->cx = j ; 
				}
				else
					E->cx = 0;
			}
			else 
				E->cx = j + 1;
		}
	}
	
}
void movecursor(editor *E, int key) {
	char *p;
	int len;
       	char *r = ((E->cy_list) >= E->numrows)?NULL:ctop(&(E->l),(E->cy_list));
	int i, l, no_tab = 0, rowlen;
	if(r) {
		l = strlen(r);
		rowlen = r ? l : 0;
		if(E->cx > rowlen)
		E->cx = rowlen;
	}
	else {
		E->cx = 0;
		l = 0;
	}
	switch(key) {
		case arrow_left:
			if(E->cx != 0) 
				(E->cx)--;
			else {
				if((E->cy_list) > 0) {
					E->cy_list--;
					E->cy--;
					p = ctop(&(E->l), (E->cy_list));
					if(p) {
						len = strlen(p);
					}
					else {
						len = 0;
					}
					E->cx = len;
				}
			}
			break;
		case arrow_right:
			if(r && E->cx < l)
				(E->cx)++;
			else {
				if(r && E->cx == l) {
					E->cy++;
					E->cy_list++;
					(E->cx) = 0;
				}
			}
			break;
		case  arrow_up:
			if((E->cy_list) != 0) {
				E->cy_list--;
				E->cy--;
			}
			break;
		case arrow_down:
			if(E->cy_list != E->numrows) {
				E->cy++;
				E->cy_list++;
			}
			break; 
		case home:
			E->cx = 0;
			break;
		case end:
			{
				if(E->cy_list <  E->numrows) {
					char *r;
					int len;
					r = ctop(&(E->l), E->cy_list);
					len = strlen(r);
					E->cx = len;
				}
			}	
			break;
		case start:
			E->cx = 0;
			E->cy = E->printstart;
			E->cy_list = 0;
			break;
		case finish:
			E->cx = 0;
			E->cy = E->numrows + E->printstart;
			E->cy_list = E->numrows;
			break;
	}
	 r = (E->cy_list >= E->numrows)?NULL:ctop(&(E->l),E->cy_list);
	if(r) {
		l = strlen(r);
	for(i = 0; i < l; i++) {
		if(r[i] == '\t')
			no_tab++;
	}
	l = l + 7*no_tab;
	rowlen = r ? l : 0;
	if(E->cx > rowlen)
		E->cx = rowlen;
	}
	else {
		E->cx = 0;
		l = 0;
	}
	
}
void open2file(editor *E) {
	E->file2 = editorPrompt(E, "enter 2nd file name %s", NULL);
	if(E->file2[0] == '1')
		E->file2 = NULL;
		E->fileflag = 1;
}
void processkeypress(editor *E){
	int c;
	int k;
	static int quit_time = 2;
	static int buffer2_time = 3;
	c=readkey(E);
	if(c == CTRL_KEY('a')) {
		k = readkey(E);
		switch(k) {
			case 'f':			//search a string in file 
				find(E);
				if(E->findout)
					searchlinesstore(E); 	//store all lines where given search is
				break;
			case 'r':	
				if(E->findout)		//replace the searched word by another word 
					replace(E);
				else
					setstatusmessage(E, "first do search!!!");
				break;
			case 'd':	
				if(E->findout)		//delete the searched word from file
				searchanddelete(E);
				else
					setstatusmessage(E,"first do search!!!");
				break;
			case 'w':			//move the cursor to begining of file
				movecursor(E,start);
				break;
			case 'e':		//move the cursor to end of file
				movecursor(E,finish);
				break;
			case 'l':			//find a particular line in file from given line no
				findline(E);
				break;
			case 's':			//split the window
				open2file(E);
				break;
			case 'c':			//switch between two windows
				E->fileflag = 1;
				break;
			default:
				break;
		
		} 
	}
	else {
		switch(c){
			case '\r'://ctrl + m			//for enter key
				insertnewline(E);
				undonewline(E);
				break;
			case CTRL_KEY('q'):/*ctrl q*/		//quit the editor
				if(E->modified && quit_time > 0) {
					setstatusmessage(E,"Warning:unsaved changes !! are u sure to quit ?""press CTRL-Q %d times to quit", quit_time);
					quit_time--;
					return;
				}
				write(STDOUT_FILENO,"\x1b[2J",4);//clear entire screen
				write(STDOUT_FILENO,"\x1b[H",3);//reposition cursor to top left
				exit(0);
				break;
			case CTRL_KEY('n'):/*ctrl n*/ 		//move to next line
				movecursor(E,arrow_down);
				break;
			case CTRL_KEY('p'):/*ctrl p*/		//move to previous line
				movecursor(E,arrow_up);
				break;
			case CTRL_KEY('f'):/*ctrl f*/
				movecursor(E,arrow_right);	//move forward by character
				break;
			case CTRL_KEY('b'):/*ctrl b*/		//move backward by character
				movecursor(E,arrow_left);
				break;
			case backspace:
				E->undodelete = 0;		
				deletechar(E);			//use backspace to delete a backward character
				break;
		        case delete:
		        	E->fromdelete = 1;
		        	E->undodelete = 0;
		        	movecursor(E,arrow_right);
		        	deletechar(E);			//use delete button to delete a forward character
		        	break;

			case page_up:case page_down:		//page up and page down
				{
					if(c == page_up) {
						E->cy = E->rowoff + E->printstart;
						E->cy_list = E->cy - E->printstart;
					}
					else if(c == page_down) {
						E->cy = E->rowoff + E->printstart + E->screenrows - 1;
						if(E->cy > E->numrows)
							E->cy = E->numrows + E->printstart;
						E->cy_list = E->cy - E->printstart;
					}
					
					int time;
					time = E->screenrows;
					while(time--) {
						movecursor(E,c == page_up ? arrow_up : arrow_down);	
					}
				}
				break;
			case CTRL_KEY('v'):/*ctrl v*/		//page down by ctrl + v
				{	
						E->cy = E->rowoff + E->printstart + E->screenrows - 1;
						if(E->cy > E->numrows)
							E->cy = E->numrows + E->printstart;
						E->cy_list = E->cy - E->printstart;
					int time;
					time = E->screenrows;
					while(time--)
						movecursor(E,arrow_down);
				
				}
				break;
			case CTRL_KEY('u'):/*ctrl u*/		//page up by ctrl + u
				{
						E->cy = E->rowoff + E->printstart;
						E->cy_list = E->cy - E->printstart;
					int time;
					time = E->screenrows;
					while(time--)
						movecursor(E,arrow_up);
				}
				break;
			case CTRL_KEY('d'):                       //delete a row
				E->undodelete = 0;
				deleterow(E);
				break;	
			
			case CTRL_KEY('c'):			//copy a line
				copyline(E);
				break;
			case CTRL_KEY('y'):			//paste a line
				pasteline(E);
				break;
			case CTRL_KEY('z'):			//undo
				undo(E);
				break;
			case CTRL_KEY('t'):                       //copy start from
				copystart(E);
				break;
			case CTRL_KEY('r'):
				copyend(E);		//copy end at
				break;
			case CTRL_KEY('e'):
				copytext(E);		//copy the text
				break;
			case CTRL_KEY('o'):
				cutstart(E);		//cut start from
				break;
			case CTRL_KEY('j'):
				cutend(E);		//cut end at
				break;
			case CTRL_KEY('x'):
				cuttext(E);		//cut the tex
				break;
			case CTRL_KEY('k'):
				pasteposition(E);	//paste at
				break;
			case CTRL_KEY('l'):
				E->frompaste = 1;
				pastetext(E,E->pastey , E->pastex, &(E->buffl));	//paste text
				break; 			
			case CTRL_KEY('g'):
				movebackwardbyword(E);		//move backward by word
				break;
			case CTRL_KEY('h'):
				moveforwardbyword(E);		//move forward by word
				break;
			case arrow_up:case arrow_down:case arrow_left:case arrow_right:case home:case end:   //all arrow keys , home & end 
				movecursor(E,c);
				break;
			case CTRL_KEY('s'):			//save the file on disk and if we have unnamed file prompt the user for 
				savefile(E);				//save as
				break;
			case '\x1b':
				break;
			case CTRL_KEY('w'):
				if(E->modified && buffer2_time > 0) {
					setstatusmessage(E,"Warning: %s unsaved changes !! are u sure to quit ?""press CTRL-g %d times to quit",E->filename ? E->filename:"[untitled file]", buffer2_time);
					buffer2_time--;
					return;
				}
				E->fileflag = 2;
				break;
			default:
				E->undoinsert = 0;		//write in editor
				insertchar(E, c);			//tab button for insertion of tab and ctrl + i for insertion of tab
				break;
			
		}
	}
	quit_time = 2;
	buffer2_time = 3;
}
/***init***/
void initEditor(editor *E) {
	E->cx = 0;//col
	E->cy = E->printstart;//row
	E->cy_list = 0;
	E->rowoff = 0;
	E->coloff = 0;
	E->numrows = 0;
	E->copybeginy = 0;
	E->copybeginx = 0;
	E->copyendy = 0;
	E->copyendx = 0;
	E->cutstarty = 0;
	E->cutstartx = 0;
	E->cutendy = 0;
	E->cutendx = 0;
	E->pastey = 0;
	E->pastex = 0;
	E->frompaste = 0;
	E->rx = 0;
	E->i = 0;
	init(&(E->buffl));
	E->buffrow = 0;
	init(&(E->l));
	sinit(&(E->s));
	sinit(&(E->s1));
	usinit(&(E->s3));
	E->d.cutstarty = INT_MIN;
	E->d.cutstartx = INT_MIN;
	E->d.cutendy = INT_MIN;
	E->d.cutendx = INT_MIN;
	E->d.pastey = INT_MIN;
	E->d.pastex = INT_MIN;
	E->d.l = NULL;
	E->filename = NULL;
	E->statusmsg[0] = '\0';
	E->statusmsg_time = 0;
	E->modified = 0;
	E->copyline = NULL;
	E->undodelete = 0;
	E->undoinsert = 0;
	E->findout = NULL;
	E->replace = NULL;
	E->searchlines = NULL;
	E->searchsize = 1000;
	E->sn = 0;
	E->undoarrayi = 0;
	E->fromdelete = 0;
	E->fileflag = 0;
	E->file2 = NULL;
	E->screenrows = E->screenrows - 2;
}
