#include"list.h"
typedef struct data {
	int cutstarty;
	int cutstartx;
	int cutendy;
	int cutendx;
	int pastey;
	int pastex;
	list *l;
}data;
typedef struct node2 {
	data d;
	struct node2 *next;
}node2;
typedef node2* ustack;
void usinit(ustack *s);
void upush(ustack *s, data d);
data upop(ustack *s);
int uisempty(ustack *s);
int uisfull(ustack *s);
