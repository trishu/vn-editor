/**editor header**/
#include<stdio.h>
#include<termios.h>
#include<unistd.h>
#include<stdlib.h>
#include<ctype.h>
#include<errno.h>
#include <sys/ioctl.h>
#include<string.h>
#include<sys/types.h>
#include<stdarg.h>
#include<time.h>
#include<limits.h>
#include"stack.h"
#include"stack2.h"
#define CTRL_KEY(K) ((K) & 0x1f) 
/***undo stack flags**/
#define flagss 10
#define flags1 11
#define flags3 12
/***detecting filetype****/
#define hl_highlight_numbers (1<<0)
#define hl_highlight_strings (1<<1)

enum highlight {
	hl_normal = 1,
	hl_number,
	hl_search,
	hl_string,
	hl_comment,
	hl_mlcomment,
	hl_keyword1,
	hl_keyword2,
	hl_preprocessor
};
typedef struct editorinfo{
	int cx;
	int cy;
	int cy_list;
	int rx;
	int screenrows;
	int screencols;
	int rowoff;
	int coloff;
	int numrows;
	int copybeginy;
	int copybeginx;
	int copyendy;
	int copyendx;
	int pastey;
	int pastex;
	int frompaste;
	int cutstarty;
	int cutstartx;
	int cutendy;
	int cutendx;
	int modified;
	list l;
	list buffl;
	int buffrow;
	stack s;
	stack s1;
	ustack s3;
	data d;
	int undodelete;
	int undoinsert;
	char* filename;
	char statusmsg[100];
	char* copyline;
	char* findout;
	char* replace;
	int searchsize;
	int* searchlines;
	int sn;
	time_t statusmsg_time;
	int undoarray[1000];
	int undoarrayi;
	int i;
	int printstart;
	int fileflag;
	char *file2;
	int fromdelete;
}editor;
/****prototypes***/
void enablerow(editor *E);
void initEditor(editor *E);
int getwindowsize(int *rows, int *cols);
void die(const char *s);
int editoropen(editor *E,char* filename);
void setstatusmessage(editor *E, const char *fmt, ...);
void refreashscreen(editor *E);
char *editorPrompt(editor *E, char *prompt, void (*callback)(editor*,char*, int));
void cuttext(editor *E);
void pastetext(editor *E, int pastey, int pastex, list *b);
void refreashscreen(editor *E);
void processkeypress(editor *E);
void refreashscreenfor2buffer(editor *E);
void showcursor(editor *E);

