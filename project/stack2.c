#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"stack2.h"
void usinit(ustack *s) {
	*s = NULL;
}
void upush(ustack *s, data d) {
	node2 *tmp = (node2 *)malloc(sizeof(node2));
	list *p = (list*)malloc(sizeof(list));
	p->head = d.l->head;
	p->tail = d.l->tail;
	d.l = p;
	tmp->d = d;
	tmp->next = *s;
	*s = tmp;
}
data upop(ustack *s) {
	node2 *tmp = *s;
	data d;
	d = (*s)->d;
	(*s) = (*s)->next;
	free(tmp);
	return d;
}
int uisempty(ustack *s) {
	return (*s) == NULL;
}
int uisfull(ustack *s) {
	return 0;
}

