#include"list.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
void init(list *l) {
	l->head = l->tail = NULL;
}
void insert(list *l, char* str, int pos) {
	int i;
	node *temp, *p;
	p = l->head;
	if(l->head == NULL && pos!= 0)
		return;
	if(pos < 0)
		return;
	for(i = 0; i<(pos - 1); i++) {
		p = p->next;
		if(p == NULL)
			return;
	}
	temp = (node*)malloc(sizeof(node));
	temp->p = (char*)malloc(strlen(str) + 1);
	strcpy(temp->p, str);
	temp->next = NULL;
	if(l->head == NULL) {
		l->head = l->tail = temp;
	}
	else if(pos == 0) {
		temp->next = l->head;
		l->head = temp;
	}
	else if(p->next == NULL) {
		p->next = temp;
		l->tail = temp;
	}
	else {
		temp->next = p->next;
		p->next = temp;
	}
}
char* remov(list *l, int pos) {
	int i;
	char* r;
	node *temp, *p, *q;
	if(l->head == NULL)
		return NULL;
	p = q = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i< pos; i++) {
		q = p;
		p = p->next;
		if(p == NULL)
			return NULL;
	}
	temp =  p;
	r = temp->p;
	if(l->head == NULL) {
		l->head = l->tail = NULL;
	}
	else if(pos == 0) {
		l->head = l->head->next;
		p->next = NULL;
	}
	else if(p->next == NULL) {
		l->tail = q;
		q->next = NULL;
	}
	else {
		q->next = p->next;
		p->next = NULL;
	}
	free(p);
	return r;
}
char* ctop(list *l, int pos) {
	node *p;
	char *r;
	int i;
	p = l->head;
	if(pos < 0)
		return NULL;
	for(i = 0; i< pos; i++) {
		p = p->next;
		if(p == NULL)
			return NULL;
	}
	r = (char*)malloc(sizeof(char)*strlen(p->p));
	strcpy(r, p->p);
	return r;
}
void printlist(list *l) {
	node *p = l->head;
	printf("fwd:");
	printf("[");
	while( p != NULL) {
		printf("%s ",p -> p);
		p = p -> next;
	}
	printf("]\n");
}
