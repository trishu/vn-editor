typedef struct node1 {
	char *p;
	int row;
	int col;
	struct node1 *next;
}node1;
typedef node1 *stack;
void sinit(stack *s);
void push(stack *s, char* str, int row, int col);
char* pop(stack *s, int* row, int* col);
int isempty(stack *s);
int isfull(stack *s);
