#include<stdio.h>
#include<math.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
double cos(double x) {
	int sign = -1, i = 1;
	double sum = 1.000, term = 1.0000, d, xsq;
	xsq = x*x;
	while(fabs(term)>1e-20) {
		d = (2*(double)i)*(2*(double)i-1);
		term = term / d;
		term = term * xsq;
		sum = sum + sign * term;
		sign = sign * -1;
		i++;
	}
	return  sum;
}
int main(int agrc, char* agrv[]) {
	int fd, fd1, k=0;
	char ch,m[100], ch1='\n';
	double res, x;
	fd = open(agrv[1], O_RDONLY);
	fd1= open(agrv[2], O_RDWR | O_CREAT);
	while(read(fd,&x, sizeof(double))) {
		read(fd, &ch, sizeof(char));
		res = cos(x);
		//printf("%lf\n",res);
		sprintf(m,"%d,%lf",k, res);
		write(fd1,m,strlen(m));
		write(fd1,&ch1,sizeof(char));
		k++;
	}
	return 0;
}
	
