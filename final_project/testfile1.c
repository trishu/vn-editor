#include <stdio.h>
#include <limits.h>
#include "stack.h"
#include "cstack.h"
#define OPERAND	100
#define OPERATOR 200
#define END	300
#define ERR 400
typedef struct token {
	int type; // OPERAND, OPERATOR or END
	int val; // will be used if type==
token gettoken(char *expr, int *reset) {
	static int i = 0;
	int no;
	char currchar;
	static enum state currstate = SPC;
	enum state nextstate;
	token t;
	if(*reset == 1) {
		currstate = SPC;
		*reset = 0;
c		i = 0;
	}
	while(1) {
		currchar = expr[i];
		//printf("currstate: %s currchar: %c\n", states[currstate], currchar);
		switch(currstate) {
			case NUMBER:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						nextstate = NUMBER;
						no = no * 10 + (currchar - '0');
						i++;
						break;
					case '+': case '-': case '*': case '/': case '%':
						nextstate = OP;
						t.type = OPERAND;
						t.val = no;
						currstate = nextstate;
						i++;
						return t;
						break;
					case '\0':
						nextstate = FINISH;
						t.type = OPERAND;
						t.val = no;
						currstate = nextstate;
						return t;
						break;
					case ' ':
						nextstate = SPC;
						t.type = OPERAND;
						t.val = no;
						currstate = nextstate;
						i++;
						return t;
						break;
					default: // anything else
						nextstate = ERROR;
						t.type = OPERAND;
						t.val = no;
						currstate = nextstate;
						return t;
						break;
				}
				break;
			case OP:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						no = currchar - '0';
						t.type = OPERATOR;
						t.op = expr[i - 1];
						nextstate = NUMBER;
						currstate = nextstate;
						i++;
						return t;
						break;
					case '+': case '-': case '*': case '/': case '%':
						nextstate = OP;
						t.type = OPERATOR;
						t.op = expr[i - 1];
						currstate = nextstate;
						i++;
						return t;
						break;
					case '\0':
						nextstate = FINISH;
						t.type = OPERATOR;
						t.op = expr[i - 1];
						currstate = nextstate;
						return t;
						break;
					case ' ':
						nextstate = SPC;
						t.type = OPERATOR;
						t.op = expr[i - 1];
						currstate = nextstate;
						i++;
						return t;
						break;
					default: // anything else
						nextstate = ERROR;
						t.type = OPERATOR;
						t.op = expr[i - 1];
						currstate = nextstate;
						return t;
					 	break;
				}
				break;
			case FINISH:
				t.type = END;
				return t;
				break;
			case ERROR:
				t.type = ERR;
				return t;
				break;
			case SPC:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						no = currchar - '0';
						nextstate = NUMBER;
						i++;
						break;
					case '+': case '-': case '*': case '/': case '%':
						nextstate = OP;
						i++;
						break;
					case '\0':
						nextstate = FINISH;
						break;
					case ' ':
						nextstate = SPC;
						i++;
						break;
					default: // anything else
						nextstate = ERROR;
						break;
				}
				currstate = nextstate;
				break;
		}
	}
}
#include "stack.h"
// + 2 3 * 2 \0
int prefix(char *expr) {
	token t;
	int x, y, z, flag = 0, reset = 1;
	stack s;
	cstack cs;
	init(&s);
	cinit(&cs);
	while(1){
		t = gettoken(expr, &reset);
		if(t.type==OPERATOR) 
			cpush(&cs,t.op);
		else if(t.type==OPERAND) {
			if(flag==1){
				x = t.val;
				y = pop(&s);
				currop = cpop(&cs);
				switch(currop) {
					case '+':
						z = y + x;
						break;
					case '*':
						z = y * x;
						break;
					case '-':
						z = y - x;
						break;
					case '/':
						z = y / x;
						break;
					case '%':
						z = y % x;
						break;
					default:
						return INT_MIN;
				}
				push(&s,z);
			}
			if(flag==0){
				flag = 1;
				push(&s, t.op);
			}		
		}
		else if(t.type == END)
			return pop(&s);
	}
} 
int readline(char *line, int len) {
	int i;
	char ch;
	i = 0;
	while(i < len - 1) {
		ch = getchar();
		if(ch == '\n') {
			line[i++] = '\0';
			return i - 1;
		}
		else
			line[i++] = ch;
	}
	line[len - 1] = '\0';
	return len - 1;
}
int main(int argc, char *argv[]) {
	char line[32];
	int r;
	while(readline(line, 32)) {
		r = prefix(line);
		if(r != INT_MIN)
			printf("%d\n", r); 
		else
			printf("error in expr\n");
	}
	return 0;
}
