#include"editorheader.h"
int getwindowsize(int *rows, int *cols) {
  struct winsize ws;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    return -1;
  } else {
    *cols = ws.ws_col;
    *rows = ws.ws_row;
    return 0;
  }
}
/*open a one file 1st and then open 2nd file , render between two files, and come back to any one file that u want
or
open 2 files first , render between two files, come back to any one file that u want*/
int main(int agrc, char* agrv[]) { 			
	editor E, E1;						
	int rows, cols, fsuccess;
	enablerow(&E);
	if(getwindowsize(&rows, &cols) == -1)
		die("getwindowsize");
	if(agrc >= 4){
		fprintf(stdout, "usage:./project filename.c\r\nusage:./project filename1.c filename2.c\r\n");
		exit(0); 
	}
	if(agrc <= 2) {						
								
		E.screenrows = rows;
		E.screencols = cols;
		E.printstart = 0;
		initEditor(&E);
		if(agrc == 2) {
			fsuccess = editoropen(&E, agrv[1]);		//if given file dosesn't exit then file of that name is created
		}
		while(1) {
			refreashscreen(&E);
			processkeypress(&E);
			if(E.fileflag == 1)
				break;
		}
		if(E.fileflag == 1) {
				E.screenrows = rows / 2;
				E.screencols = cols;
				E.printstart = 0;
				E.screenrows = E.screenrows -2;
				refreashscreen(&E);
				E1.screenrows = rows / 2;
				E1.screencols = cols;
				E1.printstart = rows / 2;
				initEditor(&E1);
				if(E.file2) {
					fsuccess = editoropen(&E1, E.file2);
				}
		}
	}
	if(agrc == 3) {
		E.screenrows = rows / 2;
		E.screencols = cols;
		E.printstart = 0;
		initEditor(&E);
		fsuccess = editoropen(&E, agrv[1]);
		E1.screenrows = rows / 2;
		E1.screencols = cols;
		E1.printstart = rows / 2;
		initEditor(&E1);
		fsuccess = editoropen(&E1, agrv[2]);
	
	}
		while(1) {	
		while(1) {
		switch(E.fileflag) {
			case 0:
			
				refreashscreen(&E);
				refreashscreenfor2buffer(&E1);
				showcursor(&E);	
				processkeypress(&E);
				if(E.fileflag == 1)
					E1.fileflag = 0;
				if(E.fileflag == 2) {
					E.fileflag = 3;
					E1.screenrows = rows;
					E1.screencols = cols;
					E1.printstart = 0;
					E1.cy = E1.printstart;
					E1.cy_list = 0;
					E1.screenrows = E1.screenrows - 2;
					E1.fileflag = 2;
				}
				break;
			case 1:
				refreashscreen(&E);
				refreashscreen(&E1);
				processkeypress(&E1);
				if(E1.fileflag == 1)
					E.fileflag  = 0;
				if(E1.fileflag == 2) {
					E.fileflag = 2;
					E.screenrows = rows;
					E.screencols = cols;
					E.printstart = 0;
					E.screenrows = E.screenrows - 2;
					
				}
				break;
			case 2:
				refreashscreen(&E);
				processkeypress(&E);
				if(E.fileflag == 1) {
					E.screenrows = rows / 2;
					E.screencols = cols;
					E.printstart = 0;
					E.screenrows = E.screenrows -2;
					refreashscreen(&E);
					E1.screenrows = rows / 2;
					E1.screencols = cols;
						E1.printstart = rows / 2;
					initEditor(&E1);
					if(E.file2) {
						fsuccess = editoropen(&E1, E.file2);
					}
				}
				break;
				
		}
		if(E.fileflag == 3)
			break;
		}
		while(1) {
			switch(E1.fileflag) {
				case 0:
					refreashscreen(&E1);
					refreashscreenfor2buffer(&E);
					showcursor(&E1);	
					processkeypress(&E1);
					if(E1.fileflag == 1)
						E.fileflag = 0;
					if(E1.fileflag == 2) {
						E1.fileflag = 3;
						E.screenrows = rows;
						E.screencols = cols;
						E.printstart = 0;
						E.cy = E.printstart;
						E.cy_list = 0;
						E.screenrows = E.screenrows - 2;
					
						E.fileflag = 2;
					}
					break;
				case 1:
					refreashscreen(&E1);
					refreashscreen(&E);
					processkeypress(&E);
					if(E.fileflag == 1)
						E1.fileflag  = 0;
					if(E.fileflag == 2) {
						E1.fileflag = 2;
						E1.screenrows = rows;
						E1.screencols = cols;
						E1.printstart = 0;
						E1.screenrows = E1.screenrows - 2;
					}
					break;
				case 2:
				refreashscreen(&E1);
				processkeypress(&E1);
				if(E1.fileflag == 1) {
					E1.screenrows = rows / 2;
					E1.screencols = cols;
					E1.printstart = 0;
					E1.screenrows = E1.screenrows -2;
					refreashscreen(&E1);
					E.screenrows = rows / 2;
					E.screencols = cols;
						E.printstart = rows / 2;
					initEditor(&E);
					if(E1.file2) {
						fsuccess = editoropen(&E, E1.file2);
					
					}
				}
				break;
			}
			if(E1.fileflag == 3)
				break;
		
		
		
		}
		}
	}
