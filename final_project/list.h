#include<stdio.h>
/***list header***/
struct syntax{
	char* filetype;
	char** filematch;
	char **keywords1;
	char **keywords2;
	char **preprocessor;
	char* ml_comment_start;
	char* ml_comment_end;
	int flags;
};



typedef struct node {
	char* p;
	char *hl;
	
	struct node* next;
}node;
typedef struct list {
	node *head, *tail;
	struct syntax* syntax1;
}list;
void init(list *l);
void insert(list *l, char* str, int pos);
char* remov(list *l, int pos);
char* ctop(list *l, int pos);
char * giverow(list *l, int pos);
int listsize(list *l);
char* ctopforcolor(list *l, int pos);
