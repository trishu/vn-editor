PROJECT NAME: vn-editor(text-editor)
NAME: mahima vijay kothari
MIS ID: 111703032

DESCRIPTION OF WORK:
	Various features of editor:
		opening an empty file,
		opening given file ,
		saving a file,
		open a two files at time-horizontally,
		rendering between two files,
		from two opened files..come back to any one file,
		from single opened file..open another file at bottom of window,
		insert a text anywhere,
		insert newline with autotab,
		use of tab,
		navigate (line up, line down, char left, char right, page up, page down, home(start of row), end(end of line), at begining 					of file, at end of file),
		navigate (by word ---forward by word, and backward by word),
		copy a line,
		paste a line,
		delete a line,
		copy any part and paste the copied part,
		cut any part and paste the cut part,
		search for any word (incremental search-- can move forward or backward by arrow keys),
		replace the searched word by another word (only those words are replaced which are present as whole single word),
		search and word and delete that word(only the whole and single word is deleted),
		given a lineno and column no ---go to that position,
		undo --(previously inserted chars/lines are deleted, deleted chars/lines inserted(not working that much properly--problem in 						deletion of newline), pasted-text is cut, cuted-text is paste),
		use of backspace,
		use of delete,
		text-colouring for .c-files and .h-files,
		no text-colouring for text-files and other types of files,
		
Description of every-features and how to use it:
	1) opening a one file and then open 2nd file horizontally ....(./project filename.c)
		press ctrl+a+s --enter filename to open and 2nd file is opened at bottom
		press ctrl+a+c --move between two files
		press ctrl+w --move back to one file(if ctrl+w is pressed in upper file then bottom file is opend in complete window and 								vice versa )	
	2)  opening two file at a time...(./project filename1.c filename2.c)
	     press ctrl+a+c : to render between 2 files
	     press ctrl+w :to move back to one file (if ctrl+w is pressed in bottom file then upper file is open in full screen & vice 									versa) 
	     (for ctrl+w if file is modified ..it warn the user to save and to quit anyways press ctrl+w twice)
	3) if filename is not given untitled file is open...(./project or ./project filename)
		if filename is given but such file does not exist then file of that name is created
	4) line up navigation----upward arrow / ctrl+p
	5) line down navigation ---downward arrow / ctrl+n
	6) move forward by char --right arrow/ ctrl+f
	7) move backward by char --left arrow / ctrl+b
	8) pageup --pgup button / ctrl+u
	9) pagedown --pgDn button / ctrl+v
	10) go to start of line --home button 
	11) go to end of line --end button
	12) go to begining of file -- ctrl+a+w
	13) go to end of file --ctrl+a+e
	14) search a word --ctrl+a+f
	15) replace a searched word by another word -- ctrl+a+r
	16) delete a searched word --ctrl+a+d
	17) go to particular line and column --ctrl+a+l
	18) insert newline --enter button /ctrl+m
	19) insert tab --tab button / ctrl + i
	20) delete previous char --backspace button
	21) delete forward char --delete
	22) delete a row --ctrl + d
	23) copy a line --ctrl+c
	24) paste the copied line --ctrl+y (without copied you can't paste)
	25) copy a text (from any where to any where but copystart position should be above copyend position)
		copy start --ctrl+t
		copy end --ctrl+r	(copy end position can't be '~' line)
		copy text --ctrl+e
	26) cut a text (from anywhere to anywhere but cutstart position should be above cutendposition)
		cut start --ctrl+o
		cut end --ctrl+j     (cut end position can't be '~' line)
		cut text --ctrl+x
	27) paste a text (at a given position)
		paste position --ctrl+k (paste position can't be '~' line)
		paste text --ctrl+l
	28) move forward by word --ctrl + h
	29) move backward by word -- ctrl+g
	30) undo --ctrl+z
		1. previously inserted chars/lines are deleted
		2. previously deleted chars/ lines are inserted 
		3. cut-text is pasted
		4. pasted-text is cut
	all undo are done in order--like whatever is done at end is undo first
		(but undo is not working that much well ...some cases are remaining)
	31) save a file  --ctrl+s
		if file is untitled then it prompt the user to enter filename
			and after giving filename to untitled file ..if filename is .c file/.h file text colouring is done immediately
	32)quit the editor --ctrl+q
		if file is modified then it warn the user ...to quit anyways press ctrl+q trice
(if editor is divided into two windows then if we do editorprompt in first file then 2nd file is vanished after the editorprompt is closed
 the 2nd file is come back ...these is because of refreashscreen in editorprompt,
 undo is not done that much properly -- some cases are remaining in inserting deleted char by backspace / delete button)
