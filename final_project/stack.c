#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"stack.h"
void sinit(stack *s) {
	*s = NULL;
}
void push(stack *s, char *str, int row, int col) {
	node1 *tmp = (node1 *)malloc(sizeof(node1));
	tmp->p = (char*)malloc(strlen(str) + 1);
	tmp->row = row;
	tmp->col = col;
	strcpy(tmp->p, str);
	tmp->next = *s;
	*s = tmp;
}
char* pop(stack *s, int *row, int* col) {
	node1 *tmp = *s;
	char* str = (*s)->p;
	*row = (*s)->row;
	*col = (*s)->col;
	(*s) = (*s)->next;
	free(tmp);
	return str;
}
int isempty(stack *s) {
	return (*s) == NULL;
}
int isfull(stack *s) {
	return 0;
}

